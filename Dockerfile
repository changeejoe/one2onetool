#define image to build from
FROM node:18-alpine3.15

#create app directory
WORKDIR /usr/src/app

#Bundle app source
COPY . .

#install app dependencies
COPY package*.json ./

RUN npm install

EXPOSE 3000

CMD [ "node", "server.js" ]
