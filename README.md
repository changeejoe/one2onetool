# DevOps CI/CD

DevOps is a simple nodejs web applications based on https://github.com/sngsweekeat/one2onetool

## Features
- Create a pipeline for CI/CD 
- Received an email notification if any of the pipieline failed (test, build, deployment)

## Tech
DevOps CI/CD uses a number of open source projects to perform CI/CD:

- [Digital Ocean] - Deployment of web app and hosting
- [GitLab] - Performing of CI/CD
- [Docker] - Building of Web app into container
- [node.js] - Web application

## Installation
- Pull the source code (Main, Bracnch: staging, release)
- Commit of any changes to any of Branch/Main will trigger the CI/CD at GitLab (https://gitlab.com/changeejoe/one2onetool)

## Triggers
- Commit via the “staging” branch will use “Questions-test.json” as its input
datafile
- Commit via the “release” branch will use “Questions.json” as its input datafile
- Commit via the main branch will use input from the env variable "DATA_FILE" as it's its input datafile

## Verification
Verify the deployment by navigating to 159.223.41.139:3000 in
your preferred browser.

## Credit
The original source code come from https://github.com/sngsweekeat/one2onetool
